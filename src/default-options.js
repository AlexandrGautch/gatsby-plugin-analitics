export const defaultOptions = {
  environments: [`production`, `development`],
  googleTagManager: {
    cookieName: `gatsby-gdpr-google-tagmanager`,
    dataLayerName: `dataLayer`,
    routeChangeEvent: `gatsbyRouteChange`
  },
  facebookPixel: {
    cookieName: `gatsby-gdpr-facebook-pixel`
  },
  tikTokPixel: {
    cookieName: `gatsby-gdpr-tiktok-pixel`
  }
}
