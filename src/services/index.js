const {
  validGTMTrackingId,
  validFbPixelId,
  validTikTokPixelId,
  getCookie
} = require('../helper')

const {
  addGoogleTagManager,
  initializeGoogleTagManager,
  trackGoogleTagManager
} = require('./google-tag-manager')

const {
  addFacebookPixel,
  initializeFacebookPixel,
  trackFacebookPixel
} = require('./facebook')

const {
  addTikTokPixel,
  initializeTikTokPixel,
  trackTikTokPixel
} = require('./tiktok')

exports.initializeAndTrackGoogleTagManager = (options, location) => {
  console.log('start initializeAndTrackGoogleTagManager');
  if (
    validGTMTrackingId(options)
  ) {
    console.log('validGTMTrackingId');
    let environmentParamStr = ``
    if (options.gtmAuth && options.gtmPreview) {
      environmentParamStr = `&gtm_auth=${options.gtmAuth}&gtm_preview=${options.gtmPreview}&gtm_cookies_win=x`
    }

    addGoogleTagManager(options, environmentParamStr).then((status) => {
      console.log('addGoogleTagManager');
      if (status) {
        initializeGoogleTagManager(options)
        trackGoogleTagManager(options, location)
      }
    })
  }
}

exports.initializeAndTrackFacebookPixel = (options) => {
  if (
    validFbPixelId(options)
  ) {
    addFacebookPixel().then((status) => {
      if (status) {
        initializeFacebookPixel(options)
        trackFacebookPixel(options)
      }
    })
  }
}

exports.initializeAndTrackTikTokPixel = (options) => {
  if (
    validTikTokPixelId(options)
  ) {
    addTikTokPixel().then((status) => {
      if (status) {
        initializeTikTokPixel(options)
        trackTikTokPixel(options)
      }
    })
  }
}