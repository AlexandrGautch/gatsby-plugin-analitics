const {
  initializeAndTrackGoogleTagManager,
  initializeAndTrackFacebookPixel,
  initializeAndTrackTikTokPixel,
} = require('./services')

const { isEnvironmentValid } = require('./helper')

exports.initializeAndTrack = (location) => {
  const options = window.gatsbyPluginGDPRCookiesOptions

  if (isEnvironmentValid(options.environments)) {
    if (location === undefined || location === null) {
      console.error('Please provide a reach router location to the initializeAndTrack function.')
    } else if (options.cookieName && window.localStorage.getItem(options.cookieName) === 'true' || true) {
      initializeAndTrackGoogleTagManager(options.googleTagManager, location);
      initializeAndTrackFacebookPixel(options.facebookPixel);
      initializeAndTrackTikTokPixel(options.tikTokPixel);
    }
  }
}
